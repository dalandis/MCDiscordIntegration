package controllers

import (
	"github.com/astaxie/beego"
	"github.com/james4k/rcon"
	"net"
	"fmt"
	"io"
	"os"
)

type MCSend struct {
	beego.Controller
}

func (this *MCSend) Post() {

	hostPort := net.JoinHostPort("", "25570")

	remoteConsole, err := rcon.Dial(hostPort, "")

	if err != nil {
		fmt.Fprintln(os.Stderr, "Failed to connect to RCON server", err)
	}
	defer remoteConsole.Close()

	preparedCmd := this.GetString("command")
	reqId, err := remoteConsole.Write(preparedCmd)

	resp, respReqId, err := remoteConsole.Read()
	if err != nil {
		if err == io.EOF {
			return
		}
		fmt.Fprintln(os.Stderr, "Failed to read command:", err.Error())
		return
	}

	if reqId != respReqId {
		fmt.Print(resp)
	}

	fmt.Print(resp)
}
