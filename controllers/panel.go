package controllers

import (
	"github.com/astaxie/beego"
)

type Panel struct {
	beego.Controller
}

func (this *Panel) Index() {
	this.Data["Website"] = "beego.me"
	this.Data["Email"]   = "astaxie@gmail.com"
}