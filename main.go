package main

import (
	_ "MCDiscordIntegration/routers"
	"github.com/astaxie/beego"
	"MCDiscordIntegration/goroutine"
)

func main() {
	go discord.Discord()
	beego.Run()
}

