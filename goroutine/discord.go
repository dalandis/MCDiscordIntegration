package discord

import (
	"github.com/bwmarrin/discordgo"
	"fmt"
)

var (
	dg *discordgo.Session
)

func Discord() {
	dg, err := discordgo.New("Bot " + "")

	if err != nil {
		fmt.Println("error creating Discord session,", err)
		return
	}

	dg.AddHandler(messageCreate)

	err = dg.Open()

	if err != nil {
		fmt.Println("error opening connection, ", err)
	}
	

	defer dg.Close()
}

func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	if m.Author.ID == s.State.User.ID {
		return
	}
	
	if m.ChannelID == "" {
		fmt.Println("messages: ", m.Content)
	}
}