package routers

import (
	"MCDiscordIntegration/controllers"
	"github.com/astaxie/beego"
)

func init() {
	beego.Router("/send/rcon", &controllers.MCSend{})
	beego.Router("/panel", &controllers.Panel{}, "get:Index")
}
