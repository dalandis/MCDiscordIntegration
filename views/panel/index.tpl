{{template "panel/layout/layout.tpl" .}}

{{define "head"}}
<title>Panel</title>
{{end}}

{{define "body"}}
<div class="container-fluid">
  <div class="card" style="width: 18rem;">
    <div class="card-body">
      <h5 class="card-title">Card title</h5>
      <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
      <a href="#" class="btn btn-primary">Go somewhere</a>
    </div>
  </div>
</div>
{{end}}