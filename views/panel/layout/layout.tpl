<!doctype html>
<html lang="en">
	<head>
		{{template "panel/layout/head.tpl" .}}
		{{template "head" .}}
	</head>
	<body>
	    {{template "panel/layout/navbar.tpl" .}}
		{{template "body" .}}
	</body>
</html>